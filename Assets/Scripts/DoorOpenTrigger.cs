using UnityEngine;

public class DoorOpenTrigger : MonoBehaviour
{
    [SerializeField] private Door _door;
    [SerializeField] private bool _isOpened = false;
    [SerializeField] private bool _hasOpener;

    private void Update()
    {
        if (_isOpened)
            return;

        if (_hasOpener)
        {
            _door.Open();
            _isOpened = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<DoorOpener>())
            _hasOpener = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<DoorOpener>())
            _hasOpener = false;
    }
}
