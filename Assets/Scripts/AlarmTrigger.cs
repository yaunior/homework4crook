using System;
using System.Collections;
using UnityEngine;

public class AlarmTrigger : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private float _audioVolumeStep;
    [SerializeField] private float _audioVolumeMax;
    [SerializeField] private float _audioVolumeMin;
    [SerializeField] private bool _isAlarm = false;
    [SerializeField] private bool _isAudioPlay;

    private void Start()
    {
        _audioSource.volume = _audioVolumeMin;
    }

    private void Update()
    {
        if (_audioSource.volume != 0 && !_isAudioPlay)
        {
            _isAudioPlay = true;
            _audioSource.Play();
        }
        
        if (_audioSource.volume == 0 && _isAudioPlay)
        {
            _isAudioPlay = false;
            _audioSource.Stop();
        }

        if (_isAlarm)
            _audioSource.volume =
                Mathf.MoveTowards(_audioSource.volume, _audioVolumeMax, _audioVolumeStep * Time.deltaTime);
        else
            _audioSource.volume =
                Mathf.MoveTowards(_audioSource.volume, _audioVolumeMin, _audioVolumeStep * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Crook>())
        {
            _isAlarm = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Crook>())
        {
            _isAlarm = false;
        }
    }
}
