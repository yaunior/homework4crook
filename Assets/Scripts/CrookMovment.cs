using System;
using UnityEngine;

public class CrookMovment : MonoBehaviour
{
    [SerializeField] private Vector3 _startPosition;
    [SerializeField] private Vector3 _endPosition;
    [SerializeField] private float _speed;

    private Vector3 _target;

    private void Start()
    {
        _target = _endPosition;
        transform.position = _startPosition;
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, _endPosition) < 0.001f)
            _target = _startPosition;
            
        transform.position = Vector3.MoveTowards(transform.position, _target, _speed * Time.deltaTime);
    }
}
